## Cactus Social Media SiteConfig Extension
SilverStripe 4.x

An extension for DataExtension to add fields for Social Media services like Facebook, YouTube, Twitter,
Instagram plus an Image upload for e.g. a logo.

---

### Setting content in CMS
The Social Media settings will be available in the CMS in 'Settings' (left hand navigation) and then under the tab "Social Media".

---

### Usage in templates

Set the scope to $SiteConfig and address the variables.

Available variables:
- $Facebook
- $YouTube 
- $Twitter
- $Instagram

Example:
```
<% with $SiteConfig %>
    <% if $Twitter %>
        {$Twitter}</a>
    <% end_if %>
<% end_with %>
```

( Last updated: 30/01/2019 )
<?php
/**
 * User: Carsten
 * Date: 30/01/2019
 * Silverstripe 4.x
 * SiteConfig extension to add social media links
 */
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\AssetAdmin\Forms\UploadField;

class SocialMediaConfig extends DataExtension
{
    private static $db = array(
        'Facebook'      => 'Varchar(100)',
        'YouTube'       => 'Varchar(100)',
        'Twitter'       => 'Varchar(100)',
        'Instagram'     => 'Varchar(100)',
        'LinkedIn'      => 'Varchar(100)',
        'TripAdvisor'   => 'Varchar',
    );

    private static $has_one = array(
        'SocialMediaImage' => Image::class
    );

    private static $owns = [
        'SocialMediaImage'
    ];

    public function updateCMSFields(FieldList $fields) {

        $fields->addFieldsToTab(
            'Root.SocialMedia',
            array(
                TextField::create("Facebook", "Facebook Page Link")
                    ->setAttribute('placeholder','e.g. https://www.facebook.com/YourPageName'),

                TextField::create("YouTube", "You Tube Channel Link")
                    ->setAttribute('placeholder','e.g. https://youtube.com/YourChannelName'),

                TextField::create("Twitter", "Your Twitter Account Link")
                    ->setAttribute('placeholder','e.g. https://twitter.com/YourTwitterName'),

                TextField::create("Instagram", "Your Instagram Account Link")
                    ->setAttribute('placeholder','e.g. https://instagram.com/YourName'),

                TextField::create("LinkedIn", "Your LinkedIn Profile Link")
                    ->setAttribute('placeholder','e.g. https://linkedin.com/in/YourName'),

                TextField::create("TripAdvisor", "Your TripAdvisor page")
                    ->setAttribute('placeholder','e.g. https://www.tripadvisor.co.nz/yourbusiness.html'),

                $upload = UploadField::create('SocialMediaImage', 'Default social media image')
                    ->setRightTitle("Allowed: jpg,jpeg,png,gif. Used when page is shared on social media.")
                    ->setAllowedMaxFileNumber(1)
                    ->setFolderName('socialmedia-images')
            ));

            $upload->getValidator()->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);                               // Validator can't be chained in addFieldsToTab
    }
}